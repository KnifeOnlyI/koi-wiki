cd /tmp/api

mvn clean package -DskipTests=true

cp target/api-*.jar /opt/koi-wiki/bin/koi-wiki-api.jar

cd /app

java -jar /opt/koi-wiki/bin/koi-wiki-api.jar