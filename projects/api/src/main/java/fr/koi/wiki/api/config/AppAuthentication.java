package fr.koi.wiki.api.config;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.List;

@Getter
@EqualsAndHashCode(callSuper = true)
public class AppAuthentication extends JwtAuthenticationToken {
    private static final String JWT_CLAIM_REALM_ACCESS = "realm_access";
    private static final String JWT_CLAIM_ROLES = "roles";
    private static final String JWT_CLAIM_USERNAME = "preferred_username";
    private static final String JWT_CLAIM_EMAIL = "email";
    private static final String JWT_CLAIM_FIRSTNAME = "given_name";
    private static final String JWT_CLAIM_LASTNAME = "family_name";

    private final List<String> roles;
    private final String username;
    private final String email;
    private final String firstname;
    private final String lastname;

    public AppAuthentication(Jwt jwt) {
        super(jwt, ((List<String>) jwt.getClaimAsMap(JWT_CLAIM_REALM_ACCESS).get(JWT_CLAIM_ROLES))
            .stream()
            .map(SimpleGrantedAuthority::new)
            .toList()
        );

        this.roles = this.getAuthorities().stream().map(GrantedAuthority::getAuthority).map(StringUtils::lowerCase).toList();
        this.username = jwt.getClaimAsString(JWT_CLAIM_USERNAME);
        this.email = jwt.getClaimAsString(JWT_CLAIM_EMAIL);
        this.firstname = jwt.getClaimAsString(JWT_CLAIM_FIRSTNAME);
        this.lastname = jwt.getClaimAsString(JWT_CLAIM_LASTNAME);
    }
}
