package fr.koi.wiki.api.config;

public final class UserRoles {
    public static final String ADMIN_ACCESS = "admin_access";
    public static final String USER_ACCESS = "user_access";

    private UserRoles() {
        // Prevent instantiation
    }
}
