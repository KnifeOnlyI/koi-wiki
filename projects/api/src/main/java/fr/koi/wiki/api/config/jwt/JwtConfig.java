package fr.koi.wiki.api.config.jwt;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

import java.net.URL;
import java.util.Optional;

@Configuration
@Slf4j
public class JwtConfig {
    @Value("${app.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuerUri;

    @Value("${app.security.oauth2.resourceserver.jwt.issuer-challenge}")
    private String issuerChallenge;

    @Bean
    public JwtDecoder jwtDecoder() {
        NimbusJwtDecoder jwtDecoder = NimbusJwtDecoder.withIssuerLocation(this.issuerUri).build();

        jwtDecoder.setJwtValidator(token -> {
            boolean issuerChallengeResult = Optional
                .ofNullable(token.getIssuer())
                .map(URL::toString)
                .map(jwtIssuer -> StringUtils.equals(jwtIssuer, this.issuerChallenge))
                .orElse(false);

            if (!issuerChallengeResult) {
                log.error("Invalid issuer challenge (expected: {}, got: {})", this.issuerChallenge, token.getIssuer());

                return OAuth2TokenValidatorResult.failure(new OAuth2Error(
                    "invalid_token",
                    "The token issuer is invalid",
                    "https://tools.ietf.org/html/rfc6750#section-3.1"
                ));
            }

            return OAuth2TokenValidatorResult.success();
        });

        return jwtDecoder;
    }
}
