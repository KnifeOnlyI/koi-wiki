package fr.koi.wiki.api.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.UuidGenerator;

import java.util.List;
import java.util.UUID;

@Entity(name = "data")
@Getter
@Setter
@Accessors(chain = true)
public class DataEntity {
    @Id
    @UuidGenerator
    private UUID id;

    private String key;

    private String value;

    @ManyToOne
    private DataEntity parent;

    @OneToMany(mappedBy = "parent")
    private List<DataEntity> children;
}
