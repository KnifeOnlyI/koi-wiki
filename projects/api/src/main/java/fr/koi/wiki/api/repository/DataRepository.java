package fr.koi.wiki.api.repository;

import fr.koi.wiki.api.entity.DataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DataRepository extends JpaRepository<DataEntity, UUID> {
}
