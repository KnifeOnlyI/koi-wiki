package fr.koi.wiki.api.service;

import fr.koi.wiki.api.config.AppAuthentication;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    public String getUsername() {
        return Optional.ofNullable(getAuthentication()).map(AppAuthentication::getUsername).orElse(null);
    }

    public String getEmail() {
        return Optional.ofNullable(getAuthentication()).map(AppAuthentication::getEmail).orElse(null);
    }

    public String getFirstname() {
        return Optional.ofNullable(getAuthentication()).map(AppAuthentication::getFirstname).orElse(null);
    }

    public String getLastname() {
        return Optional.ofNullable(getAuthentication()).map(AppAuthentication::getLastname).orElse(null);
    }

    public boolean hasRole(String role) {
        return Optional.ofNullable(getAuthentication())
            .map(authentication -> authentication.getRoles()
                .stream()
                .anyMatch(r -> StringUtils.equalsIgnoreCase(r, role))
            )
            .orElse(false);
    }

    private AppAuthentication getAuthentication() {
        return (AppAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }
}
