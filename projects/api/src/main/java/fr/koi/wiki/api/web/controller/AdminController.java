package fr.koi.wiki.api.web.controller;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import fr.koi.wiki.api.config.UserRoles;
import fr.koi.wiki.api.entity.DataEntity;
import fr.koi.wiki.api.repository.DataRepository;
import fr.koi.wiki.api.service.UserService;
import fr.koi.wiki.api.web.dto.UserInfoDto;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;

@DgsComponent
@RequiredArgsConstructor
@RolesAllowed({UserRoles.ADMIN_ACCESS})
public class AdminController {
    private final UserService userService;
    private final DataRepository dataRepository;

    @DgsQuery
    public UserInfoDto getAdminInfo() {
        return new UserInfoDto(
            this.userService.hasRole(UserRoles.USER_ACCESS),
            this.userService.hasRole(UserRoles.ADMIN_ACCESS),
            this.userService.getUsername(),
            this.userService.getEmail(),
            this.userService.getFirstname(),
            this.userService.getLastname(),
            this.dataRepository.findAll().stream().map(DataEntity::getKey).toList()
        );
    }
}
