package fr.koi.wiki.api.web.controller;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import fr.koi.wiki.api.entity.DataEntity;
import fr.koi.wiki.api.repository.DataRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@DgsComponent
@RequiredArgsConstructor
public class DataController {
    private final DataRepository dataRepository;

    @DgsQuery
    public List<DataEntity> searchData(@InputArgument String key) {
        List<DataEntity> results = this.dataRepository.findAll();

        if (key == null || StringUtils.isEmpty(key)) {
            return results;
        }

        return results.stream().filter(data -> data.getKey().contains(key)).toList();
    }
}
