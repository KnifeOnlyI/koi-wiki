package fr.koi.wiki.api.web.controller;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import org.apache.commons.lang3.StringUtils;

@DgsComponent
public class SecurityController {
    @DgsQuery
    public Boolean isAuthorized(@InputArgument String token) {
        return StringUtils.equalsIgnoreCase(token, "1234");
    }
}
