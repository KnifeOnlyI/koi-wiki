package fr.koi.wiki.api.web.dto;

import java.util.List;

public record UserInfoDto(
    boolean hasUserRole,
    boolean hasAdminRole,
    String username,
    String email,
    String firstname,
    String lastname,
    List<String> data
) {
}
