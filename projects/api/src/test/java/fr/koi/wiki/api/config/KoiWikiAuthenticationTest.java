package fr.koi.wiki.api.config;

import org.junit.jupiter.api.Test;
import org.springframework.security.oauth2.jwt.Jwt;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class KoiWikiAuthenticationTest {
    @Test
    void constructor() {
        Instant issuedAt = LocalDateTime.of(2022, 1, 1, 0, 0).atZone(ZoneId.systemDefault()).toInstant();
        Instant expiresAt = LocalDateTime.of(2022, 12, 31, 0, 0).atZone(ZoneId.systemDefault()).toInstant();
        List<String> roles = List.of("ROLE_1", "ROLE_2");
        String expectedUsername = "username";
        String expectedEmail = "email";
        String expectedFirstname = "firstname";
        String lastname = "lastname";

        AppAuthentication authentication = new AppAuthentication(new Jwt(
            "token",
            issuedAt,
            expiresAt,
            Map.of("key", "value"),
            Map.of(
                "realm_access", Map.of("roles", roles),
                "preferred_username", expectedUsername,
                "email", expectedEmail,
                "given_name", expectedFirstname,
                "family_name", lastname
            )
        ));

        assertThat(authentication.getUsername()).isEqualTo(expectedUsername);
        assertThat(authentication.getEmail()).isEqualTo(expectedEmail);
        assertThat(authentication.getFirstname()).isEqualTo(expectedFirstname);
        assertThat(authentication.getLastname()).isEqualTo(lastname);
        assertThat(authentication.getRoles()).containsExactlyInAnyOrderElementsOf(roles.stream()
            .map(String::toLowerCase)
            .toList()
        );
    }
}