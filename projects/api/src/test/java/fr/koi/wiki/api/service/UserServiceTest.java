package fr.koi.wiki.api.service;

import fr.koi.wiki.api.config.AppAuthentication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    private static final List<String> ROLES = List.of("ROLE_1", "ROLE_2");
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";

    private static final StubSecurityContext AUTHENTICATED_SECURITY_CONTEXT = new StubSecurityContext(
        LocalDateTime.of(2022, 1, 1, 0, 0),
        LocalDateTime.of(2022, 12, 31, 0, 0),
        ROLES,
        USERNAME,
        EMAIL,
        FIRSTNAME,
        LASTNAME
    );

    private static final StubSecurityContext UNAUTHENTICATED_SECURITY_CONTEXT = new StubSecurityContext();

    @InjectMocks
    private UserService userService;

    @Test
    void getDataOfAuthenticatedContext() {
        try (MockedStatic<SecurityContextHolder> utilities = Mockito.mockStatic(SecurityContextHolder.class)) {
            utilities.when(SecurityContextHolder::getContext).thenReturn(AUTHENTICATED_SECURITY_CONTEXT);

            assertThat(userService.getUsername()).isEqualTo(USERNAME);
            assertThat(userService.getEmail()).isEqualTo(EMAIL);
            assertThat(userService.getFirstname()).isEqualTo(FIRSTNAME);
            assertThat(userService.getLastname()).isEqualTo(LASTNAME);
        }
    }

    @Test
    void getDataOfUnauthenticatedContext() {
        try (MockedStatic<SecurityContextHolder> utilities = Mockito.mockStatic(SecurityContextHolder.class)) {
            utilities.when(SecurityContextHolder::getContext).thenReturn(UNAUTHENTICATED_SECURITY_CONTEXT);

            assertThat(userService.getUsername()).isNull();
            assertThat(userService.getEmail()).isNull();
            assertThat(userService.getFirstname()).isNull();
            assertThat(userService.getLastname()).isNull();
        }
    }

    @Test
    void hasRole() {
        try (MockedStatic<SecurityContextHolder> utilities = Mockito.mockStatic(SecurityContextHolder.class)) {
            utilities.when(SecurityContextHolder::getContext).thenReturn(AUTHENTICATED_SECURITY_CONTEXT);

            assertThat(userService.hasRole(ROLES.get(0).toLowerCase())).isTrue();
            assertThat(userService.hasRole(ROLES.get(0).toUpperCase())).isTrue();

            assertThat(userService.hasRole(ROLES.get(1).toLowerCase())).isTrue();
            assertThat(userService.hasRole(ROLES.get(1).toUpperCase())).isTrue();

            assertThat(userService.hasRole("ROLE_3")).isFalse();
            assertThat(userService.hasRole("role_3")).isFalse();

            assertThat(userService.hasRole(null)).isFalse();
            assertThat(userService.hasRole("")).isFalse();
        }
    }
}

class StubSecurityContext implements SecurityContext {
    private Authentication authentication = null;

    public StubSecurityContext() {
    }

    public StubSecurityContext(
        LocalDateTime issuedAt,
        LocalDateTime expiresAt,
        List<String> roles,
        String username,
        String email,
        String firstname,
        String lastname
    ) {
        this.authentication = new AppAuthentication(new Jwt(
            "token",
            issuedAt.atZone(ZoneId.systemDefault()).toInstant(),
            expiresAt.atZone(ZoneId.systemDefault()).toInstant(),
            Map.of("key", "value"),
            Map.of(
                "realm_access", Map.of("roles", roles),
                "preferred_username", username,
                "email", email,
                "given_name", firstname,
                "family_name", lastname
            )
        ));
    }

    @Override
    public Authentication getAuthentication() {
        return this.authentication;
    }

    @Override
    public void setAuthentication(Authentication authentication) {
        // Nothing to do
    }
}