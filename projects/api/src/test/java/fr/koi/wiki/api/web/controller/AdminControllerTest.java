package fr.koi.wiki.api.web.controller;

import fr.koi.wiki.api.config.UserRoles;
import fr.koi.wiki.api.entity.DataEntity;
import fr.koi.wiki.api.repository.DataRepository;
import fr.koi.wiki.api.service.UserService;
import fr.koi.wiki.api.web.dto.UserInfoDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdminControllerTest {
    @InjectMocks
    private AdminController adminController;

    @Mock
    private UserService userService;

    @Mock
    private DataRepository dataRepository;

    @Test
    void test() {
        when(userService.hasRole(UserRoles.USER_ACCESS)).thenReturn(true);
        when(userService.hasRole(UserRoles.ADMIN_ACCESS)).thenReturn(true);
        when(userService.getUsername()).thenReturn("username");
        when(userService.getEmail()).thenReturn("email");
        when(userService.getFirstname()).thenReturn("firstname");
        when(userService.getLastname()).thenReturn("lastname");
        when(dataRepository.findAll()).thenReturn(List.of(
            new DataEntity().setKey("KEY-1"),
            new DataEntity().setKey("KEY-2"),
            new DataEntity().setKey("KEY-3")
        ));

        UserInfoDto result = adminController.getAdminInfo();

        assertThat(result).isNotNull();
        assertThat(result.hasUserRole()).isTrue();
        assertThat(result.hasAdminRole()).isTrue();
        assertThat(result.username()).isEqualTo("username");
        assertThat(result.email()).isEqualTo("email");
        assertThat(result.firstname()).isEqualTo("firstname");
        assertThat(result.lastname()).isEqualTo("lastname");
        assertThat(result.data()).containsExactly("KEY-1", "KEY-2", "KEY-3");
    }
}