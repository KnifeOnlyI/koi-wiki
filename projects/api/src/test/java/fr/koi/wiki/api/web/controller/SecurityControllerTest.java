package fr.koi.wiki.api.web.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class SecurityControllerTest {
    @InjectMocks
    private SecurityController securityController;

    @Test
    void testAuthorized() {
        assertThat(this.securityController.isAuthorized("1234")).isTrue();
        assertThat(this.securityController.isAuthorized("12345")).isFalse();
    }
}