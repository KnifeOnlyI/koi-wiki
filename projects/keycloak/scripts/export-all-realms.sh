cp -rp /opt/keycloak/data/h2 /tmp

/opt/keycloak/bin/kc.sh export --dir /app/data/realms --users realm_file --db dev-file --db-url 'jdbc:h2:file:/tmp/h2/keycloakdb;NON_KEYWORDS=VALUE'

rm -rf /tmp/h2