if [[ ${IMPORT_REALMS} ]]; then
  /opt/keycloak/bin/kc.sh import --dir=/app/data/realms/ --override true
fi

/opt/keycloak/bin/kc.sh start