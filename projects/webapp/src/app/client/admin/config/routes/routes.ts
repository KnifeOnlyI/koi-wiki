import {Route} from '@angular/router';
import {AdminPageComponent} from '@admin/pages/admin-page/admin-page.component';

export const routes = new Array<Route>(
  {
    path: 'admin',
    component: AdminPageComponent
  }
);
