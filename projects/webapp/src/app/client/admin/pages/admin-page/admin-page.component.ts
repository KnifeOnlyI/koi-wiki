import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import {AppService} from '@shared/services/app/app.service';
import {toSignal} from '@angular/core/rxjs-interop';
import {JsonPipe} from '@angular/common';

@Component({
  selector: 'app-admin-page',
  template: `
    <p>admin-page works!</p>
    <pre><code>{{ isAdmin() | json }}</code></pre>
  `,
  imports: [JsonPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminPageComponent {
  readonly #appService = inject(AppService);

  protected readonly isAdmin = toSignal(this.#appService.getAdminData());
}
