import {
  ApplicationConfig,
  inject,
  provideAppInitializer,
  provideExperimentalZonelessChangeDetection
} from '@angular/core';
import {provideRouter} from '@angular/router';

import {provideClientHydration, withEventReplay} from '@angular/platform-browser';

import {provideHttpClient, withFetch, withInterceptors} from '@angular/common/http';
import {provideOAuthClient} from 'angular-oauth2-oidc';
import {UserService} from '@shared/services/user/user.service';
import {graphqlAuthHttpInterceptor} from '@shared/interceptors/http/graphql-auth-http.interceptor';
import {provideApollo} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache} from '@apollo/client/core';
import {EnvironmentService} from '@shared/services/environment/environment.service';
import {routes} from '@core/config/routes/app.routes';

export const appConfig: ApplicationConfig = {
  providers: [
    provideExperimentalZonelessChangeDetection(),
    provideRouter(routes),
    provideHttpClient(
      withFetch(),
      withInterceptors([graphqlAuthHttpInterceptor])
    ),
    provideOAuthClient(),
    provideClientHydration(withEventReplay()),
    provideAppInitializer(() => inject(UserService).loadLoggedUser()),
    // provideHttpClient(),
    provideApollo(() => {
      const httpLink = inject(HttpLink);
      const environmentService = inject(EnvironmentService);

      return {
        link: httpLink.create({
          uri: environmentService.getGraphQLUrl(),
        }),
        cache: new InMemoryCache(),
      };
    }),
  ]
};
