import {AuthConfig} from 'angular-oauth2-oidc';
import {environment} from '../../../../../environments/environment';
import {OauthConfig} from '@core/config/oauth/oauth.config';

export const authCodeFlowConfig: AuthConfig = {
  issuer: `${environment.oauth2.baseUrl}/realms/${environment.oauth2.realm}`,
  redirectUri: `${environment.webapp.baseUrl}/${OauthConfig.routeCallback}`,
  postLogoutRedirectUri: `${environment.webapp.baseUrl}`,
  clientId: environment.oauth2.clientId,
  responseType: 'code',
  scope: 'openid profile email offline_access',
  requireHttps: environment.oauth2.requireHttps,
  showDebugInformation: environment.oauth2.debug,
};
