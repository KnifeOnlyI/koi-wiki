import {Routes} from '@angular/router';
import {roleGuard} from '@shared/matchers/role.guard';
import {OauthCallbackPageComponent} from '@core/pages/oauth/oauth-callback-page/oauth-callback-page.component';
import {ErrorPageComponent} from '@core/pages/error-page/error-page.component';
import {IndexPageComponent} from '@core/pages/index-page/index-page.component';
import {OauthConfig} from '@core/config/oauth/oauth.config';

export const routes: Routes = [
  {
    path: '',
    component: IndexPageComponent,
  },
  {
    path: 'admin',
    loadChildren: () => import('@admin/config/routes/routes').then(m => m.routes),
    canMatch: [roleGuard({all: ['admin_access']})]
  },
  {
    path: 'user',
    loadChildren: () => import('@user/config/routes/routes').then(m => m.routes),
    canMatch: [roleGuard({all: ['user_access']})]
  },
  {
    path: OauthConfig.routeCallback,
    component: OauthCallbackPageComponent
  },
  {
    path: '**',
    component: ErrorPageComponent
  }
];
