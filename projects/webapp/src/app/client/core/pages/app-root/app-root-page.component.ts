import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterLink, RouterOutlet} from '@angular/router';
import {UserService} from '@shared/services/user/user.service';

@Component({
  selector: 'app-root-page',
  imports: [ReactiveFormsModule, RouterOutlet, RouterLink],
  templateUrl: './app-root-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppRootPageComponent {
  readonly #userService = inject(UserService);

  protected readonly canGoToAdminPage = this.#userService.hasRole('admin_access');
  protected readonly canGoToUserPage = this.#userService.hasRole('user_access');

  protected readonly loggedUser = this.#userService.getLoggedUser();
  protected readonly isAdmin = this.#userService.hasRole('admin_access');

  protected login() {
    this.#userService.login();
  }

  protected logout() {
    this.#userService.logout();
  }
}
