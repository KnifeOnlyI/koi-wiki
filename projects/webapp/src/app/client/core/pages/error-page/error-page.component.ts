import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-error-page',
  template: '<p>error-page works!</p>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorPageComponent {

}
