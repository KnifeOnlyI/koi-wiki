import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {AppService} from '@shared/services/app/app.service';
import {of} from 'rxjs';
import {IndexPageComponent} from '@core/pages/index-page/index-page.component';

describe('IndexPageComponent', () => {
  let fixture: ComponentFixture<IndexPageComponent>;

  let inputElement: HTMLInputElement;
  let spanElement: HTMLSpanElement;

  let appService: jasmine.SpyObj<AppService>;

  beforeEach(async () => {
    appService = jasmine.createSpyObj(AppService, ['isTokenValid']);

    await TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        {provide: AppService, useValue: appService}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(IndexPageComponent);

    inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    spanElement = fixture.debugElement.query(By.css('span')).nativeElement;

    fixture.detectChanges();
  });

  it('should display input element', () => {
    expect(inputElement).toBeTruthy();
  });

  it('should display false value in span by default', () => {
    expect(spanElement.textContent).toBe('false');
  });

  describe('input set with not valid token value', () => {
    beforeEach(async () => {
      appService.isTokenValid.and.returnValue(of(false));

      inputElement.value = 'invalid';
      inputElement.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      await fixture.whenStable();
    });

    it('should display value in span', () => {
      expect(spanElement.textContent).toBe('false');
    });
  });

  describe('input set with not empty value', () => {
    beforeEach(async () => {
      appService.isTokenValid.and.returnValue(of(true));

      inputElement.value = 'valid';
      inputElement.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      await fixture.whenStable();
    });

    it('should display value in span', () => {
      expect(spanElement.textContent).toBe('true');
    });
  });
});
