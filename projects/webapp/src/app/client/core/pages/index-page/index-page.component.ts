import {ChangeDetectionStrategy, Component, computed, inject} from '@angular/core';
import {FormControl, ReactiveFormsModule, Validators} from '@angular/forms';
import {toSignal} from '@angular/core/rxjs-interop';
import {AppService} from '@shared/services/app/app.service';
import {AsyncPipe} from '@angular/common';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-index-page',
  template: `
    <input [formControl]="tokenInput" type="text">
    <span>{{ checkTokenResult() | async }}</span>
  `,
  imports: [ReactiveFormsModule, AsyncPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexPageComponent {
  readonly #appService = inject(AppService);

  protected readonly tokenInput = new FormControl('', {validators: [Validators.required]});
  protected readonly tokenInputValue = toSignal<string | null, string>(
    this.tokenInput.valueChanges,
    {initialValue: ''}
  );
  protected readonly checkTokenResult = computed<Observable<boolean>>(() => {
    const tokenInputValue = this.tokenInputValue();

    return tokenInputValue ? this.#appService.isTokenValid(tokenInputValue) : of(false);
  });
}
