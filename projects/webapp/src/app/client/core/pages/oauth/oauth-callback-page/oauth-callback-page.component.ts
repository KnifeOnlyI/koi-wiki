import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-oauth-callback-page',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OauthCallbackPageComponent {
}
