export interface UserDto {
  firstname: string;
  lastname: string;
  roles: Array<string>;
}
