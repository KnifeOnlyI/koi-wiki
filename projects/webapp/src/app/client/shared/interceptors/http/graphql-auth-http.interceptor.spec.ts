import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {HttpClient, provideHttpClient, withInterceptors} from '@angular/common/http';
import {HttpTestingController, provideHttpClientTesting} from '@angular/common/http/testing';
import {UserService} from '@shared/services/user/user.service';
import {EnvironmentService} from '@shared/services/environment/environment.service';
import {graphqlAuthHttpInterceptor} from '@shared/interceptors/http/graphql-auth-http.interceptor';

describe('GraphQLAuthHttpInterceptor', () => {
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;
  let userService: jasmine.SpyObj<UserService>;
  let environmentService: jasmine.SpyObj<EnvironmentService>;

  beforeEach(() => {
    userService = jasmine.createSpyObj(UserService, ['getAccessToken']);
    environmentService = jasmine.createSpyObj(EnvironmentService, ['getGraphQLUrl']);

    environmentService.getGraphQLUrl.and.returnValue('http://graphql');

    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        provideHttpClient(withInterceptors([graphqlAuthHttpInterceptor])),
        provideHttpClientTesting(),
        {provide: UserService, useValue: userService},
        {provide: EnvironmentService, useValue: environmentService},
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  describe('not a GraphQL request', () => {
    it('should not add authorization header', () => {
      httpClient.get('http://rest/api').subscribe();

      const req = httpTestingController.expectOne('http://rest/api');

      expect(req.request.headers.has('Authorization')).toBeFalse();
    });
  });

  describe('graphQL request but no available token', () => {
    it('should not add authorization header', () => {
      httpClient.get('http://graphql').subscribe();

      const req = httpTestingController.expectOne('http://graphql');

      expect(req.request.headers.has('Authorization')).toBeFalse();
    });
  });

  describe('graphQL request and available token', () => {
    beforeEach(() => {
      userService.getAccessToken.and.returnValue('token');
    });

    it('should add authorization header (Bearer [token])', () => {
      httpClient.get('http://graphql').subscribe();

      const req = httpTestingController.expectOne('http://graphql');

      expect(req.request.headers.get('Authorization')).toBe('Bearer token');
    });
  });
});
