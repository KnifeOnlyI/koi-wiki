import {HttpEvent, HttpHandlerFn, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {inject} from '@angular/core';
import {EnvironmentService} from '@shared/services/environment/environment.service';
import {UserService} from '@shared/services/user/user.service';

export function graphqlAuthHttpInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> {
  const environmentService = inject(EnvironmentService);

  if (!req.url.startsWith(environmentService.getGraphQLUrl())) {
    return next(req);
  }

  const token = inject(UserService).getAccessToken();

  if (!token) {
    return next(req);
  }

  return next(req.clone({
    setHeaders: {
      Authorization: `Bearer ${token}`
    }
  }));
}
