import {TestBed} from '@angular/core/testing';
import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {UserService} from '@shared/services/user/user.service';
import {roleGuard} from '@shared/matchers/role.guard';

describe('Role Guard', () => {
  let userService: jasmine.SpyObj<UserService>;

  beforeEach(() => {
    userService = jasmine.createSpyObj(UserService, ['hasAnyRoles', 'hasAllRoles']);

    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        {provide: UserService, useValue: userService},
      ]
    });
  });

  it('hasAnyRoles true and hasAllRoles true should return true', () => {
    userService.hasAnyRoles.and.returnValue(true);
    userService.hasAllRoles.and.returnValue(true);

    const res = TestBed.runInInjectionContext(() => roleGuard({any: [], all: []})(null as any, []));

    expect(res).toBe(true);
  });

  it('hasAnyRoles false and hasAllRoles false should return false', () => {
    userService.hasAnyRoles.and.returnValue(false);
    userService.hasAllRoles.and.returnValue(false);

    const res = TestBed.runInInjectionContext(() => roleGuard({any: [], all: []})(null as any, []));

    expect(res).toBe(false);
  });

  it('hasAnyRoles false and hasAllRoles true should return false', () => {
    userService.hasAnyRoles.and.returnValue(false);
    userService.hasAllRoles.and.returnValue(true);

    const res = TestBed.runInInjectionContext(() => roleGuard({any: [], all: []})(null as any, []));

    expect(res).toBe(false);
  });

  it('hasAnyRoles true and hasAllRoles false should return false', () => {
    userService.hasAnyRoles.and.returnValue(true);
    userService.hasAllRoles.and.returnValue(false);

    const res = TestBed.runInInjectionContext(() => roleGuard({any: [], all: []})(null as any, []));

    expect(res).toBe(false);
  });
});
