import {CanMatchFn} from '@angular/router';
import {inject} from '@angular/core';
import {UserService} from '@shared/services/user/user.service';

export function roleGuard(config: {
  any?: Array<string>,
  all?: Array<string>
}): CanMatchFn {
  return () => {
    const userService = inject(UserService);

    return userService.hasAnyRoles(config.any ?? []) && userService.hasAllRoles(config.all ?? []);
  };
}
