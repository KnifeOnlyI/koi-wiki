import {TestBed} from '@angular/core/testing';
import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {Apollo, gql} from 'apollo-angular';
import {AppService} from '@shared/services/app/app.service';
import {ApolloTestingUtils} from '@shared/testing/graphql/apollo-testing';

describe('AppService', () => {
  let appService: AppService;
  let apollo: jasmine.SpyObj<Apollo>;

  beforeEach(() => {
    apollo = jasmine.createSpyObj(Apollo, ['query']);

    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        {provide: Apollo, useValue: apollo},
      ]
    });

    appService = TestBed.inject(AppService);
  });

  it('isTokenValid method should call apollo services', (done) => {
    apollo.query.withArgs({
      query: gql`{
        isAuthorized(token: "1234")
      }`,
    }).and.returnValue(ApolloTestingUtils.buildSuccessQueryResult({isAuthorized: true}));

    appService.isTokenValid('1234').subscribe((result) => {
      expect(result).toBeTrue();

      done();
    });
  });

  it('getAdminData method should call apollo services', (done) => {
    const info = {data: {}};

    apollo.query.withArgs({
      query: gql`{
        getAdminInfo {hasAdminRole, hasUserRole, username, email, firstname, lastname, data}
      }`,
    }).and.returnValue(ApolloTestingUtils.buildSuccessQueryResult({getAdminInfo: info}));

    appService.getAdminData().subscribe((result) => {
      expect(result).toBe(info);

      done();
    });
  });

  it('getUserData method should call apollo services', (done) => {
    const info = {data: {}};

    apollo.query.withArgs({
      query: gql`{
        getUserInfo {hasAdminRole, hasUserRole, username, email, firstname, lastname, data}
      }`
    }).and.returnValue(ApolloTestingUtils.buildSuccessQueryResult({getUserInfo: info}));

    appService.getUserData().subscribe((result) => {
      expect(result).toBe(info);

      done();
    });
  });
});
