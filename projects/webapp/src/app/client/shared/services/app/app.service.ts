import {inject, Injectable} from "@angular/core";
import {catchError, map, Observable, of} from 'rxjs';
import {Apollo, gql} from 'apollo-angular';

@Injectable({providedIn: 'root'})
export class AppService {
  readonly #apollo = inject(Apollo);

  isTokenValid(token: string): Observable<boolean> {
    return this.#apollo.query<{ isAuthorized: boolean }>({
      query: gql`{
        isAuthorized(token: "${token}")
      }`,
    }).pipe(
      map((result) => result.data.isAuthorized),
      catchError(() => of(false))
    );
  }

  getAdminData(): Observable<object> {
    return this.#apollo.query<{ getAdminInfo: any }>({
      query: gql`{
        getAdminInfo {hasAdminRole, hasUserRole, username, email, firstname, lastname, data}
      }`,
    }).pipe(
      map((result) => result.data.getAdminInfo),
      catchError(() => of({error: true}))
    );
  }

  getUserData(): Observable<object> {
    return this.#apollo.query<{ getUserInfo: any }>({
      query: gql`{
        getUserInfo {hasAdminRole, hasUserRole, username, email, firstname, lastname, data}
      }`,
    }).pipe(
      map((result) => result.data.getUserInfo),
      catchError(() => of({error: true}))
    );
  }
}
