import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';

export interface Environment {
  webapp: {
    baseUrl: string;
  },
  api: {
    baseUrl: string;
    graphqlEndpoint: string;
  },
  oauth2: {
    baseUrl: string;
    realm: string;
    clientId: string;
    debug: boolean;
    requireHttps: boolean;
  }
}

@Injectable({providedIn: 'root'})
export class EnvironmentService {
  getApiBaseUrl(): string {
    return environment.api.baseUrl;
  }

  getGraphQLUrl(): string {
    return `${this.getApiBaseUrl()}${environment.api.graphqlEndpoint}`;
  }
}
