import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {SessionStorageService} from '@shared/services/memory/session-storage.service';
import {PlatformService} from '@shared/services/environment/platform.service';

describe('SessionStorageService', () => {
  let sessionStorageService: SessionStorageService;
  let platformService: jasmine.SpyObj<PlatformService>;

  beforeEach(() => {
    platformService = jasmine.createSpyObj(PlatformService, ['isBrowser']);

    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        {provide: PlatformService, useValue: platformService},
      ]
    });

    sessionStorageService = TestBed.inject(SessionStorageService);
  });

  describe('not in browser', () => {
    let spyOnSessionStorageGetItem: jasmine.Spy;
    let spyOnSessionStorageSetItem: jasmine.Spy;
    let spyOnSessionStorageRemoveItem: jasmine.Spy;

    beforeEach(() => {
      platformService.isBrowser.and.returnValue(false);

      spyOnSessionStorageGetItem = spyOn(sessionStorage, 'getItem');
      spyOnSessionStorageSetItem = spyOn(sessionStorage, 'setItem');
      spyOnSessionStorageRemoveItem = spyOn(sessionStorage, 'removeItem');
    });

    it('`get` method should do nothing', () => {
      const result = sessionStorageService.get('test');

      expect(result).toBeNull();
      expect(spyOnSessionStorageGetItem).not.toHaveBeenCalled();
    });

    it('`set` method should do nothing', () => {
      sessionStorageService.set('test', 'value');

      expect(spyOnSessionStorageSetItem).not.toHaveBeenCalled();
    });

    it('`remove` method should do nothing', () => {
      sessionStorageService.remove('test');

      expect(spyOnSessionStorageRemoveItem).not.toHaveBeenCalled();
    });
  });

  describe('in browser', () => {
    let spyOnSessionStorageGetItem: jasmine.Spy;
    let spyOnSessionStorageSetItem: jasmine.Spy;
    let spyOnSessionStorageRemoveItem: jasmine.Spy;

    beforeEach(() => {
      platformService.isBrowser.and.returnValue(true);

      spyOnSessionStorageGetItem = spyOn(sessionStorage, 'getItem').withArgs('test').and.callFake(() => '{"key": "value"}');
      spyOnSessionStorageSetItem = spyOn(sessionStorage, 'setItem');
      spyOnSessionStorageRemoveItem = spyOn(sessionStorage, 'removeItem');
    });

    it('`get` method should call window API and return result', () => {
      const result = sessionStorageService.get('test');

      expect(result).toEqual({key: 'value'});
      expect(spyOnSessionStorageGetItem).toHaveBeenCalled();
    });

    it('`set` method should call window API', () => {
      sessionStorageService.set('test', JSON.stringify({key: 'value'}));

      expect(spyOnSessionStorageSetItem).toHaveBeenCalledWith('test', '"{\\"key\\":\\"value\\"}"')
    });

    it('`remove` method should call window API', () => {
      sessionStorageService.remove('test');

      expect(spyOnSessionStorageRemoveItem).toHaveBeenCalledWith('test');
    });
  });
});
