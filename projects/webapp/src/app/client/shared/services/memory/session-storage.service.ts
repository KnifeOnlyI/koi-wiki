import {inject, Injectable} from '@angular/core';
import {PlatformService} from '@shared/services/environment/platform.service';

@Injectable({providedIn: 'root'})
export class SessionStorageService {
  readonly #platformService = inject(PlatformService);

  get<T>(key: string): T | null {
    if (!this.#platformService.isBrowser()) {
      return null;
    }

    const item = sessionStorage.getItem(key);

    return item ? JSON.parse(item) : null;
  }

  set(key: string, value: any): void {
    if (this.#platformService.isBrowser()) {
      sessionStorage.setItem(key, JSON.stringify(value));
    }
  }

  remove(key: string): void {
    if (this.#platformService.isBrowser()) {
      sessionStorage.removeItem(key);
    }
  }
}
