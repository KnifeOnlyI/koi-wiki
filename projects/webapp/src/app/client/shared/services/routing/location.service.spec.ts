import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {LocationService} from '@shared/services/routing/location.service';
import {PlatformService} from '@shared/services/environment/platform.service';

describe('LocationService', () => {
  let locationService: LocationService;
  let platformService: jasmine.SpyObj<PlatformService>;

  beforeEach(() => {
    platformService = jasmine.createSpyObj(PlatformService, ['isBrowser']);

    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        {provide: PlatformService, useValue: platformService},
      ]
    });

    locationService = TestBed.inject(LocationService);
  });

  describe('not in browser', () => {
    beforeEach(() => {
      platformService.isBrowser.and.returnValue(false);
    });

    it('`getPathname` method should return null', () => {
      const res = locationService.getPathname()

      expect(res).toBeNull();
    });

    it('`getHref` method should return null', () => {
      const res = locationService.getHref()

      expect(res).toBeNull();
    });
  });
});
