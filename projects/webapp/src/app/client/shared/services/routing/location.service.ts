import {inject, Injectable} from '@angular/core';
import {PlatformService} from '@shared/services/environment/platform.service';

@Injectable({providedIn: 'root'})
export class LocationService {
  readonly #platformService = inject(PlatformService);

  getPathname(): string | null {
    if (!this.#platformService.isBrowser()) {
      return null;
    }

    return window.location.pathname;
  }

  getHref(): string | null {
    if (!this.#platformService.isBrowser()) {
      return null;
    }

    return window.location.href;
  }

  setHref(href: string): void {
    if (!this.#platformService.isBrowser()) {
      return;
    }

    window.location.href = href;
  }
}
