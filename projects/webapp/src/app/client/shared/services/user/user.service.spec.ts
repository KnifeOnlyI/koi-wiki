import {provideExperimentalZonelessChangeDetection} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {OAuthService} from 'angular-oauth2-oidc';
import {Router} from '@angular/router';
import {UserService} from '@shared/services/user/user.service';
import {PlatformService} from '@shared/services/environment/platform.service';
import {SessionStorageService} from '@shared/services/memory/session-storage.service';
import {LocationService} from '@shared/services/routing/location.service';
import {UserDto} from '@shared/dto/user/user.dto';
import {authCodeFlowConfig} from '@core/config/oauth/oauth-code-flow.config';
import {OauthConfig} from '@core/config/oauth/oauth.config';

describe('UserService', () => {
  let userService: UserService;
  let oauthService: jasmine.SpyObj<OAuthService>;
  let platformService: jasmine.SpyObj<PlatformService>;
  let sessionStorageService: jasmine.SpyObj<SessionStorageService>;
  let router: jasmine.SpyObj<Router>;
  let locationService: jasmine.SpyObj<LocationService>;

  beforeEach(() => {
    platformService = jasmine.createSpyObj(PlatformService, ['isBrowser']);
    sessionStorageService = jasmine.createSpyObj(SessionStorageService, ['get', 'set', 'remove']);
    router = jasmine.createSpyObj(Router, ['navigate']);
    locationService = jasmine.createSpyObj(LocationService, ['getPathname', 'getHref', 'setHref']);
    oauthService = jasmine.createSpyObj(OAuthService, [
      'loadDiscoveryDocumentAndTryLogin',
      'hasValidAccessToken',
      'loadUserProfile',
      'initCodeFlow',
      'logOut',
      'getAccessToken',
      'configure',
      'setupAutomaticSilentRefresh'
    ]);

    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
        {provide: OAuthService, useValue: oauthService},
        {provide: PlatformService, useValue: platformService},
        {provide: SessionStorageService, useValue: sessionStorageService},
        {provide: Router, useValue: router},
        {provide: LocationService, useValue: locationService}
      ]
    });

    userService = TestBed.inject(UserService);
  });

  describe('not in browser', () => {
    beforeEach(() => {
      platformService.isBrowser.and.returnValue(false);
    });

    it('`loadLoggedUser` method should do nothing', () => {
      let res: UserDto | null = {} as any;

      userService.loadLoggedUser().subscribe((v) => res = v);

      expect(res).toBeNull();

      expect(oauthService.configure).not.toHaveBeenCalled();
      expect(oauthService.loadDiscoveryDocumentAndTryLogin).not.toHaveBeenCalled();
    });

    it('`login` method should do nothing', () => {
      userService.login();

      expect(oauthService.configure).not.toHaveBeenCalled();
      expect(oauthService.loadDiscoveryDocumentAndTryLogin).not.toHaveBeenCalled();
      expect(oauthService.initCodeFlow).not.toHaveBeenCalled();
      expect(sessionStorageService.set).not.toHaveBeenCalled();
    });

    it('`logout` method should do nothing', () => {
      userService.logout();

      expect(oauthService.logOut).not.toHaveBeenCalled();
    });

    it('get logged user should return null', () => {
      expect(userService.getLoggedUser()).toBeNull();
    });

    it('get access token should call corresponding service', () => {
      oauthService.getAccessToken.and.returnValue('token');

      expect(userService.getAccessToken()).toBe('token');
    });

    it('has role should always return false', () => {
      expect(userService.hasRole('role')).toBe(false);
    });

    it('has any roles should always return false', () => {
      expect(userService.hasAnyRoles(['role'])).toBe(false);
    });

    it('has all roles should always return false', () => {
      expect(userService.hasAllRoles(['role'])).toBe(false);
    });
  });

  describe('in browser', () => {
    beforeEach(() => {
      platformService.isBrowser.and.returnValue(true);
      oauthService.loadDiscoveryDocumentAndTryLogin.and.returnValue(Promise.resolve(true));
    });

    describe('not logged', () => {
      it('isLogged method should return false', () => {
        expect(userService.isLogged()).toBeFalse();
      });

      it('login method should init code flow', () => {
        locationService.getHref.and.returnValue('http://localhost/login');

        userService.login();

        expect(sessionStorageService.set).toHaveBeenCalledWith('redirect-after-login', 'http://localhost/login');
        expect(oauthService.configure).toHaveBeenCalledWith(authCodeFlowConfig);
        expect(oauthService.initCodeFlow).toHaveBeenCalled();
      });

      it('logout method should call oauth logout', () => {
        userService.logout();

        expect(oauthService.logOut).toHaveBeenCalled();
      });

      it('get logged user method should return null', () => {
        expect(userService.getLoggedUser()).toBeNull();
      });

      it('role check method should return false', () => {
        expect(userService.hasRole('role')).toBeFalse();
      });

      it('any roles check method should return false', () => {
        expect(userService.hasAnyRoles(['role1', 'role2'])).toBeFalse();
      });

      it('all roles check method should return false', () => {
        expect(userService.hasAnyRoles(['role1', 'role2'])).toBeFalse();
      });

      describe('invalid access token', () => {
        beforeEach(() => {
          oauthService.hasValidAccessToken.and.returnValue(false);
        });

        it('`loadLoggedUser` method should return null because not user profile available', (done) => {
          userService.loadLoggedUser().subscribe(res => {
            expect(res).toBeNull();

            expect(oauthService.configure).toHaveBeenCalled()
            expect(oauthService.loadUserProfile).not.toHaveBeenCalled();
            expect(oauthService.setupAutomaticSilentRefresh).not.toHaveBeenCalled();
            expect(sessionStorageService.get).not.toHaveBeenCalled();
            expect(sessionStorageService.remove).not.toHaveBeenCalled();
            expect(locationService.getPathname).not.toHaveBeenCalled();
            expect(locationService.getHref).not.toHaveBeenCalled();
            expect(locationService.setHref).not.toHaveBeenCalled();
            expect(router.navigate).not.toHaveBeenCalled();

            done();
          });
        });
      });

      describe('valid access token', () => {
        beforeEach(() => {
          oauthService.hasValidAccessToken.and.returnValue(true);
          oauthService.loadUserProfile.and.returnValue(Promise.resolve({
            info: {
              given_name: 'John',
              family_name: 'Doe',
              groups: ['ROLE_USER', 'ROLE_ADMIN']
            }
          }));
        });

        describe('current location pathname is login callback', () => {
          beforeEach(() => {
            locationService.getPathname.and.returnValue(`/${OauthConfig.routeCallback}`);
          });

          describe('available redirect url in session storage', () => {
            beforeEach(() => {
              sessionStorageService.get.withArgs('redirect-after-login').and.returnValue('http://localhost/redirect');
            });

            it('should return user dto that corresponding to load user profile and redirect to saved url', (done) => {
              userService.loadLoggedUser().subscribe(res => {
                expect(res).toEqual({
                  firstname: 'John',
                  lastname: 'Doe',
                  roles: ['role_user', 'role_admin']
                });

                expect(oauthService.configure).toHaveBeenCalled()
                expect(oauthService.loadUserProfile).toHaveBeenCalled();
                expect(oauthService.setupAutomaticSilentRefresh).toHaveBeenCalled();
                expect(sessionStorageService.get).toHaveBeenCalledWith('redirect-after-login');
                expect(sessionStorageService.remove).toHaveBeenCalledWith('redirect-after-login');
                expect(locationService.getPathname).toHaveBeenCalled();
                expect(locationService.setHref).toHaveBeenCalledWith('http://localhost/redirect');
                expect(router.navigate).not.toHaveBeenCalled();

                done();
              });
            });
          });

          describe('not available redirect url in session storage', () => {
            it('should return user dto that corresponding to load user profile and redirect to index by default', (done) => {
              router.navigate.and.returnValue(Promise.resolve(true));

              userService.loadLoggedUser().subscribe(res => {
                expect(res).toEqual({
                  firstname: 'John',
                  lastname: 'Doe',
                  roles: ['role_user', 'role_admin']
                });

                expect(oauthService.configure).toHaveBeenCalled()
                expect(oauthService.loadUserProfile).toHaveBeenCalled();
                expect(oauthService.setupAutomaticSilentRefresh).toHaveBeenCalled();
                expect(sessionStorageService.get).toHaveBeenCalledWith('redirect-after-login');
                expect(sessionStorageService.remove).not.toHaveBeenCalledWith('redirect-after-login');
                expect(locationService.getPathname).toHaveBeenCalled();
                expect(locationService.setHref).not.toHaveBeenCalled();
                expect(router.navigate).toHaveBeenCalledWith(['']);

                done();
              });
            });
          });
        });
      });
    });

    describe('already logged', () => {
      beforeEach((done) => {
        oauthService.hasValidAccessToken.and.returnValue(true);
        oauthService.loadUserProfile.and.returnValue(Promise.resolve({
          info: {
            given_name: 'John',
            family_name: 'Doe',
            groups: ['ROLE_USER', 'ROLE_ADMIN']
          }
        }));

        userService.loadLoggedUser().subscribe(() => done());
      });

      it('isLogged method should return true', () => {
        expect(userService.isLogged()).toBeTrue();
      });

      it('loadLoggedUser method should return currently logged user immediatly', (done) => {
        expect(oauthService.loadDiscoveryDocumentAndTryLogin).toHaveBeenCalledTimes(1);

        userService.loadLoggedUser().subscribe((user) => {
          expect(user).toEqual({
            firstname: 'John',
            lastname: 'Doe',
            roles: ['role_user', 'role_admin']
          });

          // Expect that loadLoggedUser method will not call loadDiscoveryDocumentAndTryLogin method again
          expect(oauthService.loadDiscoveryDocumentAndTryLogin).toHaveBeenCalledTimes(1);

          done();
        });
      });

      it('login method should init code flow', () => {
        locationService.getHref.and.returnValue('http://localhost/login');

        userService.login();

        expect(sessionStorageService.set).toHaveBeenCalledWith('redirect-after-login', 'http://localhost/login');
        expect(oauthService.configure).toHaveBeenCalledWith(authCodeFlowConfig);
        expect(oauthService.initCodeFlow).toHaveBeenCalled();
      });

      it('logout method should call oauth logout', () => {
        userService.logout();

        expect(oauthService.logOut).toHaveBeenCalled();
      });

      it('get logged user method should return null', () => {
        expect(userService.getLoggedUser()).toEqual({
          firstname: 'John',
          lastname: 'Doe',
          roles: ['role_user', 'role_admin']
        })
      });

      it('role check method should return corresponding values', () => {
        expect(userService.hasRole('role_user')).toBeTrue();
        expect(userService.hasRole('hello_world')).toBeFalse();
      });

      it('any roles check method should return corresponding values', () => {
        expect(userService.hasAnyRoles([])).toBeTrue();
        expect(userService.hasAnyRoles(['role_user', 'role_admin'])).toBeTrue();
        expect(userService.hasAnyRoles(['role_user', 'hello_world'])).toBeTrue();
        expect(userService.hasAnyRoles(['good_bye', 'hello_world'])).toBeFalse();
      });

      it('all roles check method should return false', () => {
        expect(userService.hasAllRoles([])).toBeTrue();
        expect(userService.hasAllRoles(['role_user', 'role_admin'])).toBeTrue();
        expect(userService.hasAllRoles(['role_user', 'hello_world'])).toBeFalse();
        expect(userService.hasAllRoles(['good_bye', 'hello_world'])).toBeFalse();
      });
    });
  });
});
