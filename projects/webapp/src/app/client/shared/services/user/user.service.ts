import {inject, Injectable} from '@angular/core';
import {from, map, Observable, of, switchMap} from 'rxjs';
import {OAuthService} from 'angular-oauth2-oidc';
import {PlatformService} from '@shared/services/environment/platform.service';
import {SessionStorageService} from '@shared/services/memory/session-storage.service';
import {Router} from '@angular/router';
import {LocationService} from '@shared/services/routing/location.service';
import {UserDto} from '@shared/dto/user/user.dto';
import {OauthConfig} from '@core/config/oauth/oauth.config';
import {authCodeFlowConfig} from '@core/config/oauth/oauth-code-flow.config';

@Injectable({providedIn: 'root'})
export class UserService {
  private static readonly REDIRECT_AFTER_LOGIN_SESSION_STORAGE_KEY = 'redirect-after-login';

  readonly #oauthService = inject(OAuthService);
  readonly #platformService = inject(PlatformService);
  readonly #sessionStorageService = inject(SessionStorageService);
  readonly #router = inject(Router);
  readonly #locationService = inject(LocationService);

  #configured = false;
  #loggedUser: UserDto | null = null;

  isLogged(): boolean {
    return !!this.#loggedUser;
  }

  loadLoggedUser(): Observable<UserDto | null> {
    if (this.#loggedUser) {
      return of(this.#loggedUser);
    }

    if (!this.#configure()) {
      return of(null);
    }

    return from(this.#oauthService.loadDiscoveryDocumentAndTryLogin()).pipe(
      switchMap(() => this.#oauthService.hasValidAccessToken() ? from(this.#oauthService.loadUserProfile()) : of(null)),
      switchMap((userProfile: any) => {
        if (!userProfile) {
          return of(null);
        }

        const isLoginCallbackRoute = this.#locationService.getPathname() === `/${OauthConfig.routeCallback}`;

        return this.#handleLoadedUserProfile(userProfile, isLoginCallbackRoute)
      }),
    );
  }

  login(): void {
    if (this.#configure()) {
      this.#sessionStorageService.set(UserService.REDIRECT_AFTER_LOGIN_SESSION_STORAGE_KEY, this.#locationService.getHref());
      this.#oauthService.initCodeFlow();
    }
  }

  logout(): void {
    if (this.#configure()) {
      this.#oauthService.logOut();
    }
  }

  getLoggedUser(): UserDto | null {
    return this.#loggedUser;
  }

  getAccessToken(): string {
    return this.#oauthService.getAccessToken();
  }

  hasRole(role: string): boolean {
    return this.#loggedUser?.roles.includes(role.toLowerCase()) ?? false;
  }

  hasAnyRoles(roles: string[]): boolean {
    return roles.length ? roles.some(role => this.hasRole(role)) : true;
  }

  hasAllRoles(roles: string[]): boolean {
    return roles.length ? roles.every(role => this.hasRole(role)) : true;
  }

  #configure(): boolean {
    if (this.#platformService.isBrowser() && !this.#configured) {
      this.#oauthService.configure(authCodeFlowConfig);

      this.#configured = true;
    }

    return this.#configured;
  }

  #getUserProfileInfo(userProfile: { info?: object }): any {
    return userProfile?.info ?? null;
  }

  #mapUserProfileToUserDto(userProfile: any): UserDto | null {
    return userProfile ? {
      firstname: userProfile.given_name,
      lastname: userProfile.family_name,
      roles: userProfile.groups.map((role: string) => role.toLowerCase())
    } : null;
  }

  #handleLoadedUserProfile(userProfile: any, login: boolean): Observable<UserDto | null> {
    this.#loggedUser = this.#mapUserProfileToUserDto(this.#getUserProfileInfo(userProfile));

    if (this.#loggedUser) {
      this.#oauthService.setupAutomaticSilentRefresh();
    }

    if (login) {
      const redirect = this.#sessionStorageService.get<string>(UserService.REDIRECT_AFTER_LOGIN_SESSION_STORAGE_KEY);

      if (!redirect) {
        return from(this.#router.navigate(['']).then()).pipe(map(() => this.#loggedUser));
      }

      this.#sessionStorageService.remove(UserService.REDIRECT_AFTER_LOGIN_SESSION_STORAGE_KEY);
      this.#locationService.setHref(redirect);
    }

    return of(this.#loggedUser);
  }
}
