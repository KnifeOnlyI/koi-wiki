import {ApolloQueryResult, NetworkStatus} from '@apollo/client/core';
import {Observable, of} from 'rxjs';

export class ApolloTestingUtils {
  static buildSuccessQueryResult<T>(
    data: T,
    config?: {
      loading?: boolean,
      networkStatus?: NetworkStatus
    }
  ): Observable<ApolloQueryResult<T>> {
    return of({
      data,
      loading: config?.loading ?? false,
      networkStatus: config?.networkStatus ?? NetworkStatus.ready,
    });
  }
}
