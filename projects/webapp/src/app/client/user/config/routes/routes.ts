import {Route} from '@angular/router';
import {UserPageComponent} from '@user/pages/user-page/user-page.component';

export const routes = new Array<Route>(
  {
    path: 'user',
    component: UserPageComponent,
  }
);
