import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import {AppService} from '@shared/services/app/app.service';
import {toSignal} from '@angular/core/rxjs-interop';
import {JsonPipe} from '@angular/common';

@Component({
  selector: 'app-user-page',
  template: `
    <p>user-page works!</p>
    <pre><code>{{ userData() | json }}</code></pre>
  `,
  imports: [
    JsonPipe
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserPageComponent {
  readonly #appService = inject(AppService);

  protected readonly userData = toSignal(this.#appService.getUserData());
}
