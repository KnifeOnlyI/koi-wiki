// Example of environment file for production environment

import {Environment} from '../app/client/shared/services/environment/environment.service';

export const environment: Environment = {
  webapp: {
    baseUrl: 'http://localhost:8000',
  },
  api: {
    baseUrl: 'http://localhost:8001',
    graphqlEndpoint: '/graphql',
  },
  oauth2: {
    baseUrl: 'http://localhost:8002',
    realm: 'koi-wiki',
    clientId: 'webapp',
    debug: false,
    requireHttps: false
  }
};
