import { bootstrapApplication } from '@angular/platform-browser';
import { AppRootPageComponent } from './app/client/core/pages/app-root/app-root-page.component';
import { config } from './app/server/config/app.config.server';

const bootstrap = () => bootstrapApplication(AppRootPageComponent, config);

export default bootstrap;
