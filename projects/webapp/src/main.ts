import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/client/core/config/app.config';
import { AppRootPageComponent } from './app/client/core/pages/app-root/app-root-page.component';

bootstrapApplication(AppRootPageComponent, appConfig)
  .catch((err) => console.error(err));
