echo "#################################################################################################################"
echo "Gitlab Variables"
echo "#################################################################################################################"

echo "REGISTRY"
echo "├ CI_REGISTRY = '$CI_REGISTRY'"
echo "├ CI_REGISTRY_USER = '$CI_REGISTRY_USER'"
echo "├ CI_REGISTRY_IMAGE = '$CI_REGISTRY_IMAGE'"

echo "MERGE REQUEST"
echo "├ CI_MERGE_REQUEST_TITLE = '$CI_MERGE_REQUEST_TITLE'"
echo "├ CI_MERGE_REQUEST_TARGET_BRANCH_NAME = '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME'"

echo "PROJECT"
echo "├ CI_PROJECT_DIR = '$CI_PROJECT_DIR'"

echo "PIPELINE"
echo "├ CI_PIPELINE_SOURCE = '$CI_PIPELINE_SOURCE'"

echo "COMMIT"
echo "├ CI_COMMIT_TAG = '$CI_COMMIT_TAG'"

echo "#################################################################################################################"
echo "Custom Variables"
echo "#################################################################################################################"

echo "MODULE API"
echo "├ MODULE_API_ROOT_PATH = '$MODULE_API_ROOT_PATH'"
echo "├ MODULE_API_TARGET_PATH = '$MODULE_API_TARGET_PATH'"

echo "MODULE WEBAPP"
echo "├ MODULE_WEBAPP_ROOT_PATH = '$MODULE_WEBAPP_ROOT_PATH'"
echo "├ MODULE_WEBAPP_TARGET_PATH = '$MODULE_WEBAPP_TARGET_PATH'"

echo "DOCKER"
echo "├ API_IMAGE_NAME = '$API_IMAGE_NAME'"
echo "├ WEBAPP_IMAGE_NAME = '$WEBAPP_IMAGE_NAME'"